SLASH_TRYRESET1 = "/TryReset"
SLASH_TRYRESET2 = "/tr"
SlashCmdList["TRYRESET"] = function(command) 
  if command == 'help' then
    print('Description: Try to reset instances, and announce it in party/raid chat.')
    print('Usage: /tr or /TryReset')
    return
  end

  if not UnitIsGroupLeader("player") then
    return print("Cannot reset because you aren't the group leader.")
  end

  local _, instanceType = GetInstanceInfo()
  if instanceType ~= "none" then
    return print("Cannot reset because you are still inside an instance.")
  end

  ResetInstances()
  if IsInGroup() then
    SendChatMessage("All instances have been reset.", "PARTY")
  elseif IsInRaid() then
    SendChatMessage("All instances have been reset.", "RAID")
  end
  DoReadyCheck()
 end