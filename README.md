# Try Reset

Try reset adds functionality to the `ResetInstances()` function.

- Notify the user of a failed attempt to reset instances, and say why.
- Notify all party/raid members of the instance having been reset.
- Perform a ready check after successfully resetting.

# Why

Usually macros which reset the instance have multiple commands chained together,
e.g.

```
/run ResetInstances()
/p Instances have been reset.
/readycheck
```

But here are the problems with the above macro:
1. What happens if `ResetInstances` fails?
2. What if the user isn't in a party?

This addon ensures the correct chat channel is used for notifying users,
and won't perform ready checks after a failed attempt.

# Installation

1. Download this repository.
2. Place the extracted folder under your Addons directory.

# Example Usage

```
/tr
```

or

```
/TryReset
```
